package com.shop.Service.CalculatePrice;

import com.shop.Model.Shop;

public class CalculatorPriceImp implements CalculatorPrice {
    @Override
    public Shop calculatePrice(Shop shop, double amountReceived) {
        double totalPrice = shop.getPrice() * shop.getQuantity();
        double change = (amountReceived > totalPrice) ? amountReceived - totalPrice : 0.0;
        return new Shop(totalPrice, shop.getProductName(), shop.getQuantity(), change);
    }
}

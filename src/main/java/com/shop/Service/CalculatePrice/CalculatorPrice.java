package com.shop.Service.CalculatePrice;

import com.shop.Model.Shop;

public interface CalculatorPrice {
    Shop calculatePrice(Shop shop, double amountReceived);
}

package com.shop.utils;

import com.shop.Model.Shop;

public class PrintResult {
    public static void printShopInfo(Shop shop) {
        String changeMessage = (shop.getChange() > 0.0) ? "Change: $" + shop.getChange() : "";
        System.out.println("Total price of " + shop.getProductName() + " (quantity: " + shop.getQuantity() + "): $" + shop.getPrice() + " " + changeMessage);
    }
}

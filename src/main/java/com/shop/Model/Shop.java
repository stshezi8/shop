package com.shop.Model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Shop {
    public double price;
    public String productName;
    public int quantity;
    public double change;

    // Constructor
    public Shop(double price, String productName, int quantity) {
        this.price = price;
        this.productName = productName;
        this.quantity = quantity;
        this.change = 0.0;
    }
}

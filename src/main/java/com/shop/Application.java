package com.shop;

import com.shop.utils.PrintResult;
import com.shop.Model.Shop;
import com.shop.Service.CalculatePrice.CalculatorPrice;
import com.shop.Service.CalculatePrice.CalculatorPriceImp;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);


		Shop product = new Shop(10.0, "Bread", 6);
		double amountReceived = 100.0;

		CalculatorPrice calculator = new CalculatorPriceImp();

		Shop result = calculator.calculatePrice(product, amountReceived);

		PrintResult.printShopInfo(result);


	}


}
